const APIKey = '82ea82d282e80349d1002f0fb1aa2cc7';
const APIToken =
  'fd325e0dbd53939043799abfb17713916d11e453b9a3de07688a87b484a47723';
const boardId = '5e17183643c8e34e71ce284c';
listId = '5e1c104fd49b934dae76fdf5';

//get board data by particular board id
async function getBoardData(boardId) {
  const boardResponse = await fetch(
    `https://api.trello.com/1/boards/${boardId}?fields=name,url&key=${APIKey}&token=${APIToken}`
  );
  const boardData = await boardResponse.json();
  return boardData;
}
//lists data by board id
async function getListsData(boardId) {
  const listsResponse = await fetch(
    `https://api.trello.com/1/boards/${boardId}/lists?fields=name,url&key=${APIKey}&token=${APIToken}`
  );
  const listsData = await listsResponse.json();
  return listsData;
}
//cards data by listid
async function getCardsData(listId) {
  const cardsResponse = await fetch(
    `https://api.trello.com/1/lists/${listId}/cards?fields=name,url&key=${APIKey}&token=${APIToken}`
  );
  const cardsData = await cardsResponse.json();
  return cardsData;
}
//add card
async function addCard(cardName, listId) {
  const cardAddResponse = await fetch(
    `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${APIKey}&token=${APIToken}`,
    { method: 'POST' }
  );
  const addedCardData = await cardAddResponse.json();
  console.log(addedCardData);
  return addedCardData;
}

function createCard(event) {
  let listId = event.target.getAttribute('listid');
  let cardName = document.querySelector(`.card-name-holder[listId="${listId}"]`)
    .value;
  if (cardName.trim() === '') {
    alert('Please enter a card name...');
    return 0;
  }
  document.querySelector(`.card-name-holder[listId="${listId}"]`).value = '';
  addCard(cardName, listId).then(card => {
    mountCard(card, listId);
  });
}
async function deleteCard(cardId) {
  const cardDelete = await fetch(
    `https://api.trello.com/1/cards/${cardId}?key=${APIKey}&token=${APIToken}`,
    { method: 'DELETE' }
  );
  const cardDeleteResponse = await cardDelete.json();
  return cardDeleteResponse;
}

function removeCard(event) {
  let cardId = event.target.getAttribute('cardid');
  deleteCard(cardId).then(() => {
    targetCard = document.getElementById(cardId);
    targetCard.parentNode.removeChild(targetCard);
  });
}

function displayCardAddForm(event) {
  listId = event.target.getAttribute('listid');
  event.target.style.display = 'none';
  document.querySelector(`.form-add-card[listId="${listId}"]`).style.display =
    'grid';
}

function hideCardAddForm(event) {
  listId = event.target.getAttribute('listid');
  document.querySelector(
    `[class="btn add-card"][listId="${listId}"]`
  ).style.display = 'block';
  document.querySelector(`.card-name-holder[listId="${listId}"]`).value = '';
  document.querySelector(`.form-add-card[listId="${listId}"]`).style.display =
    'none';
}

//checklist
async function getChecklistsData(cardsData) {
  let checklists = [];
  for (list in cardsData) {
    for (card in cardsData[list]) {
      let checklistsQueryResponse = await fetch(
        `https://api.trello.com/1/cards/${cardsData[list][card].id}/checklists?checkItems=all&checkItem_fields=all&filter=all&fields=all&key=${APIKey}&token=${APIToken}`
      );
      let checklistsData = await checklistsQueryResponse.json();
      checklists = checklists.concat(checklistsData);
    }
  }
  return checklists;
}

async function addChecklist(checklistName, cardId) {
  const addChecklistQueryResponse = await fetch(
    `https://api.trello.com/1/cards/${cardId}/checklists?name=${checklistName}&key=${APIKey}&token=${APIToken}`,
    { method: 'POST' }
  );
  const addedChecklistData = await addChecklistQueryResponse.json();
  console.log(addedChecklistData);
  return addedChecklistData;
}

function createChecklist(event) {
  const cardId = event.target.getAttribute('cardId');
  const checklistName = document.querySelector(
    `.checklist-name-holder[cardId="${cardId}"]`
  ).value;
  if (checklistName.trim() == '') {
    alert('Please enter a checklist name...');
    return 0;
  }
  document.querySelector(`.checklist-name-holder[cardId="${cardId}"]`).value =
    '';
  addChecklist(checklistName, cardId).then(checklist => {
    mountChecklist(checklist);
  });
}

async function deleteChecklist(checklistId) {
  const deleteChecklistQueeryResponse = await fetch(
    `https://api.trello.com/1/checklists/${checklistId}?key=${APIKey}&token=${APIToken}`,
    { method: 'DELETE' }
  );
  const checklistDeleteResponse = deleteChecklistQueeryResponse.json();
  return checklistDeleteResponse;
}

function removeChecklist(event) {
  const checklistId = event.target.getAttribute('checklistId');
  deleteChecklist(checklistId).then(() => {
    const targetChecklist = document.getElementById(checklistId);
    targetChecklist.parentNode.removeChild(targetChecklist);
  });
}

//checklist item

async function addChecklistItem(checkItemName, checklistId) {
  const addCheckItemQueryResponse = await fetch(
    `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${checkItemName}&pos=bottom&checked=false&key=${APIKey}&token=${APIToken}`,
    { method: 'POST' }
  );
  const addedCheckItemData = addCheckItemQueryResponse.json();
  console.log(addedCheckItemData);
  return addedCheckItemData;
}

function createChecklistItem(event) {
  const checklistId = event.target.getAttribute('checklistId');
  const cardId = event.target.getAttribute('cardId');
  const checkItemName = document.querySelector(
    `.checklist-item-name-holder[checklistId="${checklistId}"]`
  ).value;
  if (checkItemName.trim() == '') {
    alert('please enter a checklist item name...');
    return 0;
  }
  document.querySelector(
    `.checklist-item-name-holder[checklistId="${checklistId}"]`
  ).value = '';
  addChecklistItem(checkItemName, checklistId).then(checkItem => {
    mountChecklistItem(checkItem, cardId);
  });
}

async function deleteChecklistItem(checkItemId, checklistId) {
  const deleteCheckItemQueryResponse = await fetch(
    `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${APIKey}&token=${APIToken}`,
    { method: 'DELETE' }
  );
  const deleteCheckItemResponse = deleteCheckItemQueryResponse.json();
  return deleteCheckItemResponse;
}

function removeChecklistItem(event) {
  const checkItemId = event.target.getAttribute('checkItemId');
  const checklistId = event.target.parentNode.getAttribute('checklistId');
  deleteChecklistItem(checkItemId, checklistId).then(() => {
    const targetCheckItem = document.getElementById(checkItemId);
    targetCheckItem.parentNode.removeChild(targetCheckItem);
  });
}

function mountBoard(board) {
  document.getElementById('board-name').innerHTML = board.name;
}

function mountList(list) {
  let listBox = document.createElement('div');
  listBox.setAttribute('id', list.id);
  listBox.classList.add('list-box');
  let listLabel = document.createElement('div');
  listLabel.classList.add('list-label');
  let listName = document.createElement('span');
  listName.classList.add('list-name');
  listName.innerHTML = list.name;
  let delBtn = document.createElement('button');
  let ul = document.createElement('ul');
  ul.classList.add('list');
  ul.setAttribute('listId', list.id);
  let addCardFormBtn = document.createElement('button');
  addCardFormBtn.classList.add('btn', 'add-card');
  addCardFormBtn.setAttribute('listId', list.id);
  addCardFormBtn.innerHTML = 'Add New Card';
  addCardFormBtn.addEventListener('click', displayCardAddForm);
  let cardAddForm = document.createElement('div');
  cardAddForm.classList.add('form-add-card');
  cardAddForm.setAttribute('listId', list.id);
  let cardNameBox = document.createElement('input');
  cardNameBox.setAttribute('type', 'text');
  cardNameBox.classList.add('card-name-holder');
  cardNameBox.setAttribute('listId', list.id);
  let addCardBtn = document.createElement('button');
  addCardBtn.classList.add('btn', 'add-card');
  addCardBtn.setAttribute('listId', list.id);
  addCardBtn.innerHTML = 'Add Card';
  addCardBtn.addEventListener('click', createCard);
  let cancelBtn = document.createElement('button');
  cancelBtn.classList.add('btn', 'cancel');
  cancelBtn.setAttribute('listId', list.id);
  cancelBtn.innerHTML = 'cancel';
  cancelBtn.addEventListener('click', hideCardAddForm);
  cardAddForm.appendChild(cardNameBox);
  cardAddForm.appendChild(addCardBtn);
  cardAddForm.appendChild(cancelBtn);
  listLabel.appendChild(listName);
  listBox.appendChild(listLabel);
  listBox.appendChild(ul);
  listBox.appendChild(addCardFormBtn);
  listBox.appendChild(cardAddForm);
  document.getElementById('list-holder').appendChild(listBox);
}
function mountCard(card, listId) {
  let li = document.createElement('li');
  let cardBox = document.createElement('div');
  cardBox.classList.add('card-box');
  cardBox.setAttribute('id', card.id);
  let cardElement = document.createElement('div');
  cardElement.classList.add('card-label');
  let cardContent = document.createElement('span');
  cardContent.classList.add('card-content');
  cardContent.innerHTML = card.name;
  let delBtn = document.createElement('button');
  delBtn.classList.add('btn');
  delBtn.setAttribute('cardId', card.id);
  delBtn.innerHTML = 'cancel';
  delBtn.addEventListener('click', removeCard);
  cardElement.appendChild(cardContent);
  cardElement.appendChild(delBtn);
  let ul = document.createElement('ul');
  ul.classList.add('checklist-list');
  ul.setAttribute('cardId', card.id);
  let checklistAddForm = document.createElement('div');
  checklistAddForm.classList.add('form-add-checklist');
  checklistAddForm.setAttribute('cardId', card.id);
  let checklistNameHolder = document.createElement('input');
  checklistNameHolder.setAttribute('cardId', card.id);
  checklistNameHolder.setAttribute('type', 'text');
  checklistNameHolder.classList.add('checklist-name-holder');
  checklistNameHolder.placeholder = 'Type new checklist name here...';
  let addChecklistButton = document.createElement('button');
  addChecklistButton.setAttribute('cardId', card.id);
  addChecklistButton.classList.add('btn', 'add-checklist');
  addChecklistButton.innerHTML = 'Add Checklist';
  addChecklistButton.addEventListener('click', createChecklist);
  checklistAddForm.appendChild(checklistNameHolder);
  checklistAddForm.appendChild(addChecklistButton);
  cardBox.appendChild(cardElement);
  cardBox.appendChild(ul);
  cardBox.appendChild(checklistAddForm);
  li.appendChild(cardBox);
  document.querySelector(`.list[listId="${listId}"]`).appendChild(li);
}
function mountChecklist(checklist) {
  let li = document.createElement('li');
  let checklistBox = document.createElement('div');
  checklistBox.setAttribute('id', checklist.id);
  checklistBox.classList.add('checklist-box');
  let checklistLabel = document.createElement('div');
  checklistLabel.classList.add('checklist-label');
  let checklistName = document.createElement('span');
  checklistName.classList.add('checklist-name');
  checklistName.innerHTML = checklist.name;
  let delBtn = document.createElement('button');
  delBtn.classList.add('btn');
  delBtn.innerHTML = 'cancel';
  delBtn.setAttribute('checklistId', checklist.id);
  delBtn.addEventListener('click', removeChecklist);
  checklistLabel.appendChild(checklistName);
  checklistLabel.appendChild(delBtn);
  checklistBox.appendChild(checklistLabel);
  let ul = document.createElement('ul');
  ul.setAttribute('checklistId', checklist.id);
  ul.classList.add('checklist');
  checklistBox.appendChild(ul);
  let checklistItemAddForm = document.createElement('div');
  checklistItemAddForm.classList.add('form-add-checklist-item');
  let checklistItemNameHolder = document.createElement('input');
  checklistItemNameHolder.setAttribute('type', 'text');
  checklistItemNameHolder.setAttribute('checklistId', checklist.id);
  checklistItemNameHolder.classList.add('checklist-item-name-holder');
  checklistItemNameHolder.placeholder = 'New Checklist Item...';
  let addChecklistItemButton = document.createElement('button');
  addChecklistItemButton.classList.add('btn', 'add-checklist-item');
  addChecklistItemButton.setAttribute('checklistId', checklist.id);
  addChecklistItemButton.setAttribute('cardId', checklist.idCard);
  addChecklistItemButton.innerHTML = 'Add Check Item';
  addChecklistItemButton.addEventListener('click', createChecklistItem);
  checklistItemAddForm.appendChild(checklistItemNameHolder);
  checklistItemAddForm.appendChild(addChecklistItemButton);
  checklistBox.appendChild(checklistItemAddForm);
  li.appendChild(checklistBox);
  document.querySelector(`ul[cardId="${checklist.idCard}"]`).appendChild(li);
}

function mountChecklistItem(checklistItem, cardId) {
  let li = document.createElement('li');
  let checkItem = document.createElement('div');
  checkItem.classList.add('checklist-item');
  checkItem.setAttribute('id', checklistItem.id);
  checkItem.setAttribute('checklistId', checklistItem.idChecklist);
  let checkItemLabel = document.createElement('label');
  checkItemLabel.classList.add('checklist-item-name');
  checkItemLabel.setAttribute('checkItemId', checklistItem.id);
  checkItemLabel.setAttribute('cardId', cardId);
  let checkLabel = document.createTextNode(checklistItem.name);
  let checkbox = document.createElement('input');
  checkbox.setAttribute('type', 'checkbox');
  checkbox.classList.add('check-item');
  checkbox.setAttribute('checkItemId', checklistItem.id);
  if (checklistItem.state == 'complete') {
    checkbox.checked = true;
    checkItemLabel.style.textDecoration = 'line-through';
  } else {
    checkbox.checked = false;
    checkItemLabel.style.textDecoration = 'none';
  }
  let checkboxCustom = document.createElement('span');
  checkboxCustom.classList.add('checkmark');
  checkItemLabel.appendChild(checkLabel);
  checkItemLabel.appendChild(checkbox);
  checkItemLabel.appendChild(checkboxCustom);
  let delBtn = document.createElement('button');
  delBtn.classList.add('btn');
  delBtn.setAttribute('checkItemId', checklistItem.id);
  delBtn.innerHTML = 'cancel';
  delBtn.addEventListener('click', removeChecklistItem);
  checkItem.appendChild(checkItemLabel);
  checkItem.appendChild(delBtn);
  li.appendChild(checkItem);
  document
    .querySelector(`ul[checklistId="${checklistItem.idChecklist}"]`)
    .appendChild(li);
}
async function firstThingsFirst() {
  const boardData = await getBoardData(boardId);
  mountBoard(boardData);
  const listsData = await getListsData(boardId);
  listsData.forEach(list => {
    mountList(list);
  });
  const cardsData = await getCardsData(listId);

  cardsData.forEach(card => {
    mountCard(card, listId);
  });
  const checklistsData = await getChecklistsData(cardsData);
  checklistsData.forEach(checklist => {
    mountChecklist(checklist);
  });
  checklistsData.forEach(checklist => {
    checklist.checkItems.forEach(checkItem => {
      mountChecklistItem(checkItem, checklist.idCard);
    });
  });
}
document.addEventListener('DOMContentLoaded', firstThingsFirst);